﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoresApi.Domain.Entities
{
    public class BatchExpiryDate
    {
        public int BatchExpiryDateId { get; set; }

        public int ItemId { get; set; }
        public Batch Batch { get; set; }

        public DateTime ExpiryDate { get; set; }
    }
}
