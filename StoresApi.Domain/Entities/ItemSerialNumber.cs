﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoresApi.Domain.Entities
{
    public class ItemSerialNumber
    {
        public int ItemSerialNumberId { get; set; }

        public int BatchId { get; set; }
        public Batch Batch { get; set; }

        public string SerialNumber { get; set; }
    }
}
