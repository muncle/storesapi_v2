﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoresApi.Domain.Entities
{
    public class StoreRoom
    {
        public int StoreRoomId { get; set; }
        public string Store { get; set; }
        public string StoreName { get; set; }

        public ICollection<Item> Items { get; set; }
    }
}
