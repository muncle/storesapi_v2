﻿using System;
using Microsoft.EntityFrameworkCore;
using StoresApi.Domain.Entities;

namespace StoresApi.Persistence
{
    public class StoresApiDbContext : DbContext
    {
        public StoresApiDbContext(DbContextOptions<StoresApiDbContext> options)
            : base(options)
        {

        }

        public DbSet<Item> Items { get; set; }
        public DbSet<Batch> Batches { get; set; }
        public DbSet<BatchExpiryDate> BatchExpiryDates { get; set; }
        public DbSet<ItemSerialNumber> ItemSerialNumbers { get; set; }
        public DbSet<StoreRoom> StoreRooms { get; set; }
        public DbSet<User> Users { get; set; }

    }
}
