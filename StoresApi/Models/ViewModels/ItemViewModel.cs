﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoresApi.Models.ViewModels
{
    public class ItemViewModel
    {
        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public string ItemRemarks { get; set; }
    }
}
