﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoresApi.Models.ViewModels
{
    public class StockCheckViewModel
    {
        public string ItemNumber { get; set; }
        public string ItemDescription { get; set; }
        public string BatchNumber { get; set; }
        public string ItemLocation { get; set; }
        public int BatchQuantity { get; set; }
        public string SerialNumber { get; set; }
        public DateTime ExpiryDate { get; set; }
    }
}
